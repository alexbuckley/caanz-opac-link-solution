package C4::Output::JSONStream;
#
# Copyright 2008 LibLime
#
# This file is part of Koha.
#
# Koha is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# Koha is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Koha; if not, see <http://www.gnu.org/licenses>.

=head1 NAME

C4::Output::JSONStream - progressively build JSON data

=head1 SYNOPSIS

my $json = new C4::Output::JSONStream;

$json->param( issues => [ 'yes!', 'please', 'no', { emphasis = 'NO' } ] );
$json->param( stuff => 'realia' );

print $json->output;

=head1 DESCRIPTION

This module allows you to build JSON incrementally.

=cut

use strict;
use warnings;

use JSON;

sub new {
    my $class = shift;
    my $self = {
        data => {},
        options => {}
    };

    bless $self, $class;

    return $self;
}

sub param {
    my $self = shift;

    if ( @_ % 2 != 0 ) {
        die 'param() received odd number of arguments (should be called with param => "value" pairs)';
    }

    for ( my $i = 0; $i < $#_; $i += 2 ) {
        $self->{data}->{$_[$i]} = $_[$i + 1];
    }
}

sub output {
    my $self = shift;

    return to_json( $self->{data} );
}

=head 2 clear

    $json->clear();

This clears any in-progress data from the object so it can be used to create
something new. Parameters are kept, it's just the data that goes away.

=cut

sub clear {
    my $self = shift;
    $self->{data} = {};
}

=head2 content_type

    my $ct = $json->content_type();

Returns a string containing the content type of the stuff that this class
returns, suitable for passing to L<C4::JSONStream::output_with_http_headers>.
In this case, it says 'json'.

=cut

sub content_type {
    return 'json';
}

=head2 true

    my $true = $json->true();

This provides a 'true' value, as some format types need a special value.

=cut

sub true {
    return JSON::true;
}


1;
