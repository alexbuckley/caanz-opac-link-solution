package C4::Output::XMLStream;

# Copyright 2014 Catalyst IT
#
# This file is part of Koha.
#
# Koha is free software; you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3 of the License, or (at your option) any later
# version.
#
# Koha is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with Koha; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

=head1 NAME

C4::Output::XMLStream - progressively build XML data

=head1 SYNOPSIS

    my $xml = C4::Output::XMLStream->new(root => 'rootnodename');
    
    $json->param( issues => [ 'yes!', 'please', 'no', { emphasis = 'NO' } ] );
    $json->param( stuff => 'realia' );
    
    print $json->output;

=head1 DESCRIPTION

This module makes it easy to progressively build up data structure that can
then be output as XML.

The XML output is very simple, as this is an analogue to JSON output. If
params are set thus:

    my $json = C4::Output::XMLStream->new(root => 'rootnodename');
    $json->param( issues => [ 'yes!', 'please', 'no', { emphasis = 'NO' } ] );
    $json->param( stuff => 'realia' );

Then the XML will be:

    <rootnodename>
        <issues>
            <array>
                <item>yes!</item>
                <item>please</item>
                <item>no</item>
                <emphasis>NO</emphasis>
            </array>
        </issues>
        <stuff>realia</stuff>
    </rootnodename>

It's wordy because it's XML. You can't set attributes on nodes, because you
can't do that in JSON. You can't have text elements and sub-elements at the
same time, because that's fiddly to express in Perl structures, and you can't
do that in JSON anyway.

If you want multiple subnodes, then do this:

    $xml->param(stuff => { one => 'a', two => 'b' });
    
The resulting order of the output is not particularly deterministic, given
this more or less emulates a hash, except the bits with arrays.

If you add a new param with the same name as an already existing one, the new
one will overwrite the old one.

=head1 FUNCTIONS

=cut

use Carp;
use Modern::Perl;
use XML::Writer;

=head2 new

    my $xml = C4::Output::XMLStream->new(root => 'rootnodename');

Creates a new instance of the C<C4::Output::XMLStream> class.

=cut

sub new {
    my $class = shift;
    my %opts = @_;
    croak "no root node specified" unless $opts{root};
    my $self = {
        data => {},
        %opts,
    };
    bless $self, $class;
    return $self;
}

=head2 param

    $xml->param(key1 => $value1, key2 => $value2, ...);

This adds the supplied key/value pairs to the object for later output. See
the Description section for details on what goes in vs. what comes out.

=cut

sub param {
    my $self = shift;

    if (@_ % 2 != 0) {
        croak "param() received an odd number of args, should be a list of hashes";
    }

    my %p = @_;
    # Merge the hashes, this'll overwrite any old values with new ones
    @{ $self->{data} }{ keys %p } = values %p;
}

=head2 output

    my $str = $xml->output();

Turns the provided params into an XML document string.

=cut

sub output {
    my $self = shift;

    my $str;
    my $writer = XML::Writer->new(OUTPUT => \$str);
    $writer->xmlDecl('UTF-8');
    $writer->startTag($self->{root});
    _add_to_xml($writer, $self->{data});
    $writer->endTag($self->{root});
    $writer->end();
    return $str;
}

=head2 clear

    $xml->clear();

This clears any in-progress data from the object so it can be used to create
something new. Parameters are kept, it's just the data that goes away.

=cut

sub clear {
    my $self = shift;
    $self->{data} = {};
}

=head2 content_type

    my $ct = $xml->content_type();

Returns a string containing the content type of the stuff that this class
returns, suitable for passing to L<C4::JSONStream::output_with_http_headers>.
In this case, it says 'xml'.

=cut

sub content_type {
    return 'xml';
}

=head2 true

    my $true = $xml->true();

This provides a 'true' value, as some format types need a special value.

=cut

sub true {
    return '1';
}


# This is an internal class sub that recurses through the provided data adding
# each layer to the writer.
sub _add_to_xml {
    my ($writer, $data) = @_;

    my $r = ref $data;

    if ($r eq 'HASH') {
        # data is a hashref
        while (my ($k, $v) = each %$data) {
            $writer->startTag($k);
            _add_to_xml($writer, $v);
            $writer->endTag($k);
        }
    } elsif ($r eq 'ARRAY') {
        # data is an arrayref
        $writer->startTag('array');
        foreach my $v (@$data) {
            if (ref($v) eq 'HASH') {
                # we don't say "item" for these ones
                _add_to_xml($writer, $v);
            } else {
                $writer->startTag('item');
                _add_to_xml($writer, $v);
                $writer->endTag('item');
            }
        }
        $writer->endTag('array');
    } elsif ($r eq '') {
        # data is a scalar
        $writer->characters($data);
    } else {
        confess "I got some data I can't handle: $data";
    }
}

1;

=head1 AUTHORS

=over 4

=item Robin Sheat <robin@catalyst.net.nz>

=back

=cut
