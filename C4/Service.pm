package C4::Service;
#
# Copyright 2008 LibLime
#
# This file is part of Koha.
#
# Koha is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# Koha is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Koha; if not, see <http://www.gnu.org/licenses>.

=head1 NAME

C4::Service - functions for JSON webservices.

=head1 SYNOPSIS

my $response = C4::Output::XMLStream->new(...);
my $service = C4::Service->new( { needed_flags => { circulate => 1 },
    [ output_stream => $response ],
    [ query => CGI->new() ] } );
my ( $borrowernumber) = $service->require_params( 'borrowernumber' );

$service->return_error( 'internal', 'Frobnication failed', frobnicator => 'foo' );

$response->param( frobnicated => 'You' );

C4::Service->return_success();

=head1 DESCRIPTION

This module packages several useful functions for webservices.

=cut

use strict;
use warnings;

use CGI qw ( -utf8 );
use Carp;
use C4::Auth qw( check_api_auth );
use C4::Output qw( :ajax );
use C4::Output::JSONStream;

our $debug;

BEGIN {
    $debug = $ENV{DEBUG} || 0;
}

=head1 METHODS

=head2 new

    my $service = C4::Service->new({needed_flags => { parameters => 1 }, 
        [ output_stream => C4::Output::XMLStream->new(...) ],
        [ query => CGI->new() ]});

Creates a new instance of C4::Service. It verifies that the provided flags
are met by the current session, and aborts with an exit() call if they're
not. It also accepts an instance of C4::Output::* (or something with the 
same interface) to use to generate the output. If none is provided, then
a new instance of L<C4::Output::JSONStream> is created. Similarly, a query
may also be provided. If it's not, a new CGI one will be created.

This call can't be used to log a user in by providing a userid parameter, it
can only be used to check an already existing session.

TODO: exit sucks, make a better way.

=cut

sub new {
    my $class = shift;

    my %opts = %{shift()};

    my $needed_flags = $opts{needed_flags};
    croak "needed_flags is a required option" unless $needed_flags;

    my $query = $opts{query} || CGI->new();
    # We capture the userid so it doesn't upset the auth check process
    # (if we don't, the auth code will try to log in with the userid
    # param value.)
    my $userid;
    $userid = $query->param('userid');
    $query->delete('userid') if defined($userid);

    my ( $status, $cookie, $sessionID ) = check_api_auth( $query, $needed_flags );

    # Restore userid if needed
    $query->param(-name=>'userid', -value=>$userid) if defined($userid);

    my $output_stream = $opts{output_stream} || C4::Output::JSONStream->new();
    my $self = {
        needed_flags  => $needed_flags,
        query         => $query,
        output_stream => $output_stream,
        cookie        => $cookie,
    };
    bless $self, $class;
    $self->return_error('auth', $status) if ($status ne 'ok');

    return $self;
}

=head2 return_error

    $service->return_error( $type, $error, %flags );

Exit the script with HTTP status 400, and return a JSON error object.

C<$type> should be a short, lower case code for the generic type of error (such
as 'auth' or 'input').

C<$error> should be a more specific code giving information on the error. If
multiple errors of the same type occurred, they should be joined by '|'; i.e.,
'expired|different_ip'. Information in C<$error> does not need to be
human-readable, as its formatting should be handled by the client.

Any additional information to be given in the response should be passed as
param => value pairs.

=cut

sub return_error {
    my ( $self, $type, $error, %flags ) = @_;

    my $response = $self->{output_stream};
    $response->clear();

    $response->param( message => $error ) if ( $error );
    $response->param( type => $type, %flags );

    output_with_http_headers $self->{query}, $self->{cookie}, $response->output, $response->content_type, '400 Bad Request';

    # Someone please delete this
    exit;
}

=head2 return_multi

    $service->return_multi( \@responses, %flags );

return_multi is similar to return_success or return_error, but allows you to
return different statuses for several requests sent at once (using HTTP status
"207 Multi-Status", much like WebDAV). The toplevel hashref (turned into the
JSON response) looks something like this:

    { multi => JSON::true, responses => \@responses, %flags }

Each element of @responses should be either a plain hashref or an arrayref. If
it is a hashref, it is sent to the browser as-is. If it is an arrayref, it is
assumed to be in the same form as the arguments to return_error, and is turned
into an error structure.

All key-value pairs %flags are, as stated above, put into the returned JSON
structure verbatim.

=cut

sub return_multi {
    my ( $self, $responses, @flags ) = @_;

    my $response = $self->{output_stream};
    $response->clear();

    if ( !@$responses ) {
        $self->return_success( $response );
    } else {
        my @responses_formatted;

        foreach my $response ( @$responses ) {
            if ( ref( $response ) eq 'ARRAY' ) {
                my ($type, $error, @error_flags) = @$response;

                push @responses_formatted, { is_error => $response->true(), type => $type, message => $error, @error_flags };
            } else {
                push @responses_formatted, $response;
            }
        }

        $response->param( 'multi' => $response->true(), responses => \@responses_formatted, @flags );
        output_with_http_headers $self->{query}, $self->{cookie}, $response->output, $response->content_type, '207 Multi-Status';
    }

    exit;
}

=head2 return_success

    $service->return_success();

Print out the information in the provided C<output_stream>, then
exit with HTTP status 200. To get access to the C<output_stream>, you should
either use the one that you provided, or you should use the C<output_stream()>
accessor.

=cut

sub return_success {
    my ( $self ) = @_;

    my $response = $self->{output_stream};
    output_with_http_headers $self->{query}, $self->{cookie}, $response->output, $response->content_type;
}

=head2 output_stream

    $service->output_stream();

Provides the output stream object that is in use so that data can be added
to it.

=cut

sub output_stream {
    my $self = shift;

    return $self->{output_stream};
}

=head2 query

    $service->query();

Provides the query object that this class is using.

=cut

sub query {
    my $self = shift;

    return $self->{query};
}

=head2 require_params

    my @values = $service->require_params( @params );

Check that each of of the parameters specified in @params was sent in the
request, then return their values in that order.

If a required parameter is not found, send a 'param' error to the browser.

=cut

sub require_params {
    my ( $self, @params ) = @_;

    my @values;

    for my $param ( @params ) {
        $self->return_error( 'params', "Missing '$param'" ) if ( !defined( $self->{query}->param( $param ) ) );
        push @values, $self->{query}->param( $param );
    }

    return @values;
}

=head2 dispatch

    $service->dispatch(
        [ $path_regex, \@required_params, \&handler ],
        ...
    );

dispatch takes several array-refs, each one describing a 'route', to use the
Rails terminology.

$path_regex should be a string in regex-form, describing which methods and
paths this route handles. Each route is tested in order, from the top down, so
put more specific handlers first. Also, the regex is tested on the request
method, plus the path. For instance, you might use the route [ 'POST /', ... ]
to handle POST requests to your service.

Each named parameter in @required_params is tested for to make sure the route
matches, but does not raise an error if one is missing; it simply tests the next
route. If you would prefer to raise an error, instead use
C<C4::Service->require_params> inside your handler.

\&handler is called with each matched group in $path_regex in its arguments. For
example, if your service is accessed at the path /blah/123, and you call
C<dispatch> with the route [ 'GET /blah/(\\d+)', ... ], your handler will be called
with the argument '123'.

=cut

sub dispatch {
    my $self = shift;

    my $query = $self->{query};
    my $path_info = $query->path_info || '/';

    ROUTE: foreach my $route ( @_ ) {
        my ( $path, $params, $handler ) = @$route;

        next unless ( my @match = ( ($query->request_method . ' ' . $path_info)   =~ m,^$path$, ) );

        for my $param ( @$params ) {
            next ROUTE if ( !defined( $query->param ( $param ) ) );
        }

        $debug and warn "Using $path";
        $handler->( @match );
        return;
    }

    $self->return_error( 'no_handler', '' );
}

1;

__END__

=head1 AUTHORS

Koha Development Team

Jesse Weaver <jesse.weaver@liblime.com>
