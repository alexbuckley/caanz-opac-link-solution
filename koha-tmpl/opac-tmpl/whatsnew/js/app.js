'use strict';

// application constants
var d = new Date();
var archiveBeginningMonth = d.getMonth() + 7  ;
var archiveBeginningYear =  d.getFullYear() - 1;
var lastUpdated = document.documentElement.getAttribute('data-lastUpdated');
var baseReportUrl = '/cgi-bin/koha/svc/report'; // id for the report gets set in each call in controllers.js
var baseUrl = '/whatsnew'; // note the href for the banner url in index.html needs configuring too if this value changes

// add .trim() and .indexOf() for IE8 compatibility
if(!String.prototype.trim){
  String.prototype.trim = function(){
    return this.replace(/^\s+|\s+$/g,'');
  };
}
if (!Array.prototype.indexOf) {
    Array.prototype.indexOf = function(obj, start) {
         for (var i = (start || 0), j = this.length; i < j; i++) {
             if (this[i] === obj) { return i; }
         }
         return -1;
    }
}

// Declare app level module which depends on filters, and services
angular.module('informedProfessional', [
  'ngRoute',
  'informedProfessional.filters',
  'informedProfessional.directives',
  'informedProfessional.controllers',
  'ui.scrollfix'
]).
config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/:locationSetting/', {templateUrl: 'partials/informed-professional-main.html', controller: 'newletterController', 'reloadOnSearch': false});
  $routeProvider.when('/:locationSetting/archive/:currentYear/:currentMonth', {templateUrl: 'partials/informed-professional-main.html', controller: 'newletterController', 'reloadOnSearch': false});
  $routeProvider.otherwise({redirectTo: '/all/'});
}]).
run(['$rootScope', function($rootScope) {
  $rootScope.archiveBeginningMonth = archiveBeginningMonth;
  $rootScope.archiveBeginningYear = archiveBeginningYear;
  $rootScope.lastUpdated = lastUpdated;
  $rootScope.baseReportUrl = baseReportUrl;
  $rootScope.baseUrl = baseUrl + '/#';
}]);
