'use strict';

/* Filters */

angular.module('informedProfessional.filters', [])
.filter('anchorEncode', [function() {
    return function(text) {
        return String(text).replace(/ /g, "-").toLowerCase().trim();
    }
}])
.filter('removeTitleColon', [function() {
    return function(text) {
        var trimmedText = String(text).trim();
        if (trimmedText.charAt(trimmedText.length - 1) === ':') {
            return trimmedText.substr(0,trimmedText.length - 2).trim();
        }
        return trimmedText;
    }
}])
.filter('removeAuthorSlash', [function() {
    return function(text) {
        var trimmedText = String(text).trim();
        if (trimmedText.charAt(0) === '/') {
            return trimmedText.substr(1,trimmedText.length - 1).trim();
        }
        return trimmedText;
    }
}])
.filter('monthZeroFill', [function() {
    return function(text) {
        if (text.toString().length === 1) {
            text = '0' + text;
        }
        return text.toString();
    }
}])
.filter('exactMatch', [function() {
    return function(array, fields) {
        var output = [];
        var type = fields.type.split('|');

        // for each item, check if there is a match on both subject and type

        // type can either be a single item as a string, or a pipe
        // delimited list of items that it is permissable to match against

        angular.forEach(array, function(arrayItem) {
            if (fields.subject === arrayItem.subject) {
                if (type.indexOf(arrayItem.type) !== -1) {
                    output.push(arrayItem);
                }
            }
        });
        return output;
    }
}])
.filter('isbn13to10', [function() {
    return function(text){
        if ((/^978/).test(text)){
            var start = text.substring(3,12);
            var sum = 0;
            var mul = 10;
            var i;
            for(i = 0; i < 9; i++) {
                sum = sum + (mul * parseInt(start[i]));
                mul -= 1;
            }
            var checkDig = 11 - (sum % 11);
            if (checkDig == 10) {
                 checkDig = "X";
            } else if (checkDig == 11) {
                 checkDig = "0";
            }
            return start + checkDig;
       }
       else {
            return text;
       }
   }
}]);
