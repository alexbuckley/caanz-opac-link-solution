'use strict';

/* Directives */


angular.module('informedProfessional.directives', [])
.directive('coverImg', ['$rootScope', function($rootScope) {
    return {
        priority: 99, // it needs to run after the attributes are interpolated
        restrict: 'A', // this is an attribute directive
        link: function(scope, element, attr) {
            if (attr.coverImg) {
                attr.$set('src', '/cgi-bin/koha/opac-image.pl?biblionumber=' + attr.biblionumber + '&thumbnail=1');
            } else {
                element.load(function() { // wait for the amazon image to load
                    if (element.width() < 5) { // amazon returns a 1x1 pixel if there is no cover for that ISBN. setting the threshold to 4 instead because of chrome weirdness.
                        attr.$set('src', 'img/noimage.png');
                        attr.$set('class', 'no-cover');
                        attr.$set('width', '44px');
                    }
                });
                attr.$set('src', attr.amazonCover);
            }
        }
    };
}]);
