'use strict';

/* Controllers */

angular.module('informedProfessional.controllers', []).
controller('newletterController', ['$sce','$scope', '$http', '$location', '$anchorScroll', '$routeParams', '$timeout', '$filter', function($sce, $scope, $http, $location, $anchorScroll, $routeParams, $timeout, $filter) {

    // DATE SETUP

    // parse the date info for the most recent issue
    $scope.mostRecentYear = parseInt($filter('date')($scope.lastUpdated, 'yyyy'), 10);
    $scope.mostRecentMonth = parseInt($filter('date')($scope.lastUpdated, 'M'), 10);

    // if an archive year and month is specified, use those in preference to the most recent year/month
    if ($routeParams.currentYear && $routeParams.currentMonth) {
        $scope.currentYear = parseInt($routeParams.currentYear, 10);
        $scope.currentMonth = parseInt($routeParams.currentMonth, 10);

        // we need to check that the provided values aren't later than mostRecent
        if ($scope.currentYear > $scope.mostRecentYear) {
            // coerce to the most recent they are allowed to see
            $scope.currentYear = $scope.mostRecentYear;
        }
        if ($scope.currentYear === $scope.mostRecentYear && $scope.currentMonth > $scope.mostRecentMonth) {
            // coerce to the most recent they are allowed to see
            $scope.currentMonth = $scope.mostRecentMonth;
        }
    } else {
        $scope.currentYear = $scope.mostRecentYear;
        $scope.currentMonth = $scope.mostRecentMonth;
    }
    var reportParameter = 'TOOLS';

    // LOCATION SETUP

    var locationValues = {
        'new-zealand': 'NZ',
        'australia': 'AU',
    }
    if ($routeParams.locationSetting && locationValues[$routeParams.locationSetting]) {
        $scope.locationSetting = $routeParams.locationSetting;
    } else {
        // default value
        $scope.locationSetting = 'australia';
    }
    reportParameter = reportParameter + ' - ' + locationValues[$scope.locationSetting];

    // As per WR239782 International only has access to eBooks rather than Books proper
    if ($scope.locationSetting == 'international') {
        $scope.ebooksOnly = true;
    }
    // PAGE ANCHORS
    // normal anchors are replaced with a ?jumpTo=[id] format so as to not conflict with the # url routing

    $scope.scrollTo = function(id) {
        var old = $location.hash();
        id = $filter('anchorEncode')(id);

        $location.hash(id);
        $location.search({'jumpTo': id}); // so the user can copy the url to share
        $anchorScroll();
        // reset to old value to keep any additional routing logic from kicking in
        $location.hash(old);
    };

    // DATA
    
    // editable sections
//    $http.get($scope.baseReportUrl, {
//        'params': {
//          'id': 138,  //the production report number is 138            
//            'id': 132, // the test server is 132 (for now)
//            'annotated': 1
//        }
//    }).
//    success(function(data) {
//        $scope.editables = data;
//        angular.forEach($scope.editables, function(editable){
//            if (editable.variable == 'whatsnew_links') {
//               $scope.Links = $sce.trustAsHtml(editable.value);
//            }
//            else if (editable.variable == 'whatsnew_tips') {
//               $scope.Tips = $sce.trustAsHtml(editable.value);
//            }
//       });
//    });
    // featured items
    $http.get($scope.baseReportUrl, {
        'params': {
            'id': 147,
            'sql_params': reportParameter,
            'annotated': 1
        }
    }).
    success(function(data) {
        $scope.featuredItems = data;
    });

    // items
    $http.get($scope.baseReportUrl, {
        'params': {
            'id': 146,
            'sql_params': reportParameter,
            'annotated': 1
        }
    }).
    success(function(data) {
        $scope.items = data;
        $scope.itemsLoaded = true;
        $scope.bookSubjects = [];
        $scope.articleSubjects = [];
        $scope.otherSubjects = [];

        angular.forEach($scope.items, function(item){
            if (item.type === 'ARTICLE') {
                if (jQuery.inArray(item.subject, $scope.articleSubjects) === -1) {
                    $scope.articleSubjects.push(item.subject);
                }
            } else if (item.type === 'BOOK' || item.type === 'AUCONF' || item.type === 'NZICACP' || item.type === 'CONFERENCE' || item.type === 'EBOOKSUM' ) {
                if (jQuery.inArray(item.subject, $scope.bookSubjects) === -1) {
                    $scope.bookSubjects.push(item.subject);
                }
            } else {
                if (jQuery.inArray(item.subject, $scope.otherSubjects) === -1) {
                    $scope.otherSubjects.push(item.subject);
                }
            }
        });

        // if a particular location was passed in by the user, jump to it now the page has rendered
        function pageRenderTimeout() {
            $scope.scrollTo($routeParams.jumpTo);
        }
        if ($routeParams.jumpTo) {
            $timeout(pageRenderTimeout, 200); // timeout to ensure the elements have rendered so we can scroll to them
        }
    })
    .error(function() {
        $scope.loadError = true;
    });

    // ARCHIVES AND DATE FORMATTING

    $scope.archiveMonths = [12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1];
    $scope.archiveYears = [];
    // set up year array for the archive to iterate through
    for (var year = $scope.mostRecentYear; year >= $scope.archiveBeginningYear; year -= 1) {
        $scope.archiveYears.push(year);
    }

    $scope.monthFormat = function(month) {
        // make a fake timestamp to format with the date filter
        return $filter('date')('0000' + $filter('monthZeroFill')(month) + '-01T00:00', 'MMMM');
    }

    $scope.monthDisplayCheck = function(month, currentYear, oldestYear) {
        if (currentYear && (month <= $scope.mostRecentMonth)) {
            if ($scope.mostRecentYear == $scope.archiveBeginningYear) {
                if (month < $scope.archiveBeginningMonth) {
                    return false;
                }
            }
            return true;
        } else if (oldestYear && (month >= $scope.archiveBeginningMonth) && !currentYear) {
            return true;
        } else if (!currentYear && !oldestYear) {
            return true;
        }
        return false;
    }

    // TYPES

    $scope.typeActions = {
        'VIDEO'     : 'Watch',
        'PODCAST'   : 'Listen',
        'BLOG'      : 'Browse',
        'REPORT'    : 'Download',
        'MULTIMEDIA': 'Request',
	'AUCONF'    : 'Request',
        'NZICACP'   : 'Request',
        'CONFERENCE': 'Request',
        'EBOOKSUM'  : 'Download',
        'WEB'       : 'Browse',
        'TOOLS PUB' : 'Download',
        'TOOLS'     : 'Download',
        'TOOLONLINE': 'Browse',
        'TOOLONLPRI': 'Browse'



    }

}]);
