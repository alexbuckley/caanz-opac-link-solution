#!/usr/bin/perl

# Copyright 2015 Catalyst IT
#
# This file is part of Koha.
#
# Koha is free software; you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3 of the License, or (at your option) any later
# version.
#
# Koha is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with Koha; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

use Modern::Perl;

use DBD::Mock;
use Test::MockModule;

use Test::More tests => 3;

my $module_context = new Test::MockModule('C4::Context');
my $_dbh;
$module_context->mock(
    '_new_dbh',
    sub {
        my $dbh = $_dbh // DBI->connect('DBI:Mock:', '', '') || die "Cannot create handle: $DBI::errstr\n";
        $_dbh = $dbh;
        return $dbh;
    }
);

# We need to mock the Auth process so that we can pretend we have a valid session
my $module_auth = new Test::MockModule('C4::Auth');
$module_auth->mock(
    'check_api_auth',
    sub {
        return ('ok', '', '');
    }
);

# Instead of actually outputting to stdout, we catch things on the way past
my @_out_params;
my $module_output = new Test::MockModule('C4::Output');
$module_output->mock(
    'output_with_http_headers',
    sub {
        @_out_params = @_;
    }
);

use_ok('C4::Output::XMLStream');
use_ok('C4::Service');

# Do a simple round trip test of data in to data out
my $xml_stream = C4::Output::XMLStream->new(root => 'test');

my $service = C4::Service->new( { needed_flags => { borrowers => 1 } ,
        output_stream => $xml_stream });

$service->output_stream->param( foo => 'bar' );

$service->return_success();
like($_out_params[2], qr|<test><foo>bar</foo></test>|, 'XML output generated');


