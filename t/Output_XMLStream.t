# Copyright 2014 Catalyst IT
#
# This file is part of Koha.
#
# Koha is free software; you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3 of the License, or (at your option) any later
# version.
#
# Koha is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with Koha; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

use Modern::Perl;
use Test::More tests => 15;

BEGIN {
    use_ok('C4::Output::XMLStream');
}

my $xml = C4::Output::XMLStream->new(root => 'root');

is(trim($xml->output), '<?xml version="1.0" encoding="UTF-8"?>
<root></root>
', 'Making sure that blank XML can be output');

$xml->param( issues => [ 'yes!', 'please', 'no' ] );
is(trim($xml->output),'<?xml version="1.0" encoding="UTF-8"?>
<root><issues><array><item>yes!</item><item>please</item><item>no</item></array></issues></root>
',"Making sure XML output has added what we told it to.");

$xml->param( stuff => ['realia'] );
like($xml->output,qr|<stuff><array><item>realia</item></array></stuff>|,"Making sure XML output has added more params correctly.");
like($xml->output,qr|<issues><array><item>yes!</item><item>please</item><item>no</item></array></issues>|,"Making sure the old data is still in there");

$xml->param( stuff => ['fun','love'] );
like($xml->output,qr|<stuff><array><item>fun</item><item>love</item></array></stuff>|,"Making sure XML output can overwrite params");
like($xml->output,qr|<issues><array><item>yes!</item><item>please</item><item>no</item></array></issues>|,"Making sure the old data is still in there");

$xml->param( halibut => { cod => 'bass' } );
like($xml->output,qr|<halibut><cod>bass</cod></halibut>|,"Adding of hashes works");
like($xml->output,qr|<issues><array><item>yes!</item><item>please</item><item>no</item></array></issues>|,"Making sure the old data is still in there");


eval{$xml->param( die )};
ok($@,'Dies');

eval{$xml->param( die => ['yes','sure','now'])};
ok(!$@,'Dosent die.');

eval{$xml->param( die => ['yes','sure','now'], die2 =>)};
ok($@,'Dies.');

$xml->clear();
is(trim($xml->output), '<?xml version="1.0" encoding="UTF-8"?>
<root></root>
', 'Making sure that clearing it clears it.');

is($xml->content_type, 'xml', 'Correct content type is returned');

is($xml->true, '1', 'True is true.');

sub trim {
    $_ = shift;
    $_ =~ s/^\s*(.*?)\s*$/$1/;
    return $_;
}
