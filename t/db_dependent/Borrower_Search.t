#!/usr/bin/perl

# Copyright 2015 Catalyst IT
#
# This file is part of Koha.
#
# Koha is free software; you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3 of the License, or (at your option) any later
# version.
#
# Koha is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with Koha; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

use C4::Context;

use Test::More tests => 10;

use_ok('Koha::Borrower::Search');

my $dbh = C4::Context->dbh;
$dbh->{RaiseError} = 1;
$dbh->{AutoCommit} = 0;

# Insert some borrower detail to fetch later
$dbh->do(q|INSERT INTO branches (branchcode) VALUES ('TBRANCH')|);
$dbh->do(q|INSERT INTO categories (categorycode) VALUES ('TCAT')|);
$dbh->do(q|INSERT INTO borrowers (borrowernumber,cardnumber,branchcode,categorycode,surname) VALUES (200000,'test123','TBRANCH','TCAT','Surname1'), (200001,'test124','TBRANCH','TCAT','Surname2'),(200002,'test125','TBRANCH','TCAT','Surname3')|);

# An ext attribute
$dbh->do(q|INSERT INTO borrower_attribute_types (code, description) VALUES ('TEXTATTR', 'Test Ext Attrib')|);
$dbh->do(q|INSERT INTO borrower_attributes (borrowernumber, code, attribute) VALUES (200000, 'TEXTATTR', 'TESTING')|);
$dbh->do(q|INSERT INTO borrower_attributes (borrowernumber, code, attribute) VALUES (200001, 'TEXTATTR', 'TESTING')|);
$dbh->do(q|INSERT INTO borrower_attributes (borrowernumber, code, attribute) VALUES (200002, 'TEXTATTR', 'TESTING2')|);

# And now the actual testing

# Check to make sure a couple of the columns come through.
my @cols = Koha::Borrower::Search::get_borrower_fields();
ok((grep{$_ eq 'cardnumber'} @cols), 'Borrower field 1');
ok((grep{$_ eq 'lost'} @cols), 'Borrower field 2');
ok((grep{$_ eq 'userid'} @cols), 'Borrower field 3');

# Find a borrower by a value
my $num = Koha::Borrower::Search::find_borrower(surname => 'Surname2');
is($num, 200001, 'Fetch single borrower by field');

my @nums = Koha::Borrower::Search::find_borrower(branchcode => 'TBRANCH');
is_deeply(\@nums, [200000, 200001, 200002], 'Fetch multiple borrowers by field');

# Find by ext attr
$num = Koha::Borrower::Search::find_borrower_from_ext(TEXTATTR => 'TESTING2');
is ($num, 200002, 'Fetch single borrower by ext attr');

my @nums = Koha::Borrower::Search::find_borrower_from_ext(TEXTATTR => 'TESTING');
is_deeply(\@nums, [200000, 200001], 'Fetch multiple borrowers by ext attr');

# Check that they correctly fail

my $fail = 1;
eval {
    $num = Koha::Borrower::Search::find_borrower(branchcode => 'TBRANCH');
    $fail = 0;
};
is($fail, 1, 'Raised exception for multiple results by field');

$fail = 1;
eval {
    $nums = Koha::Borrower::Search::find_borrower_from_ext(TEXTATTR => 'TESTING');
    $fail = 0;
};
is($fail, 1, 'Raised exception for multiple results by ext attr');

$dbh->rollback;
