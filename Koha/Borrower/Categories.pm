package Koha::Borrower::Categories;

# Copyright 2015 Catalyst IT
#
# This file is part of Koha.
#
# Koha is free software; you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3 of the License, or (at your option) any later
# version.
#
# Koha is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with Koha; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

use C4::Context;

use Carp;
use Date::Calc qw( Today Add_Delta_Days );

=head1 NAME 

Koha::Borrower::Categories - functions for working with borrower category information

=head1 SYNOPSIS

 use Koha::Borrower::Categories;
 $expiry = Koha::Borrower::Categories::GetExpiryDate($catcode, $datefrom);

=head1 DESCRIPTION

This is a place for functions that involve dealing with borrower categories.

=head1 FUNCTIONS

=head2 get_expiry_date

 my $expiry = Koha::Borrower::Categories::get_expiry_date($catcode, [$datefrom]);

This calculates the expiry date for a given category. If no origin date is
provided, then it is taken from C<now>. Provided dates must be ISO8601 form,
and that's what will be returned.

=cut

sub get_expiry_date {
    my ($catcode, $datefrom) = @_;

    croak "No categorycode specified" unless $catcode;

    my @datefrom = $datefrom ? split /-/, $datefrom : Today();
    my $dbh = C4::Context->dbh;
    my $sth = $dbh->prepare('SELECT enrolmentperiod,enrolmentperioddate FROM categories WHERE categorycode=?');
    $sth->execute($catcode);
    $enrolments = $sth->fetchrow_hashref;
    if ($enrolments->{enrolmentperiod}) {
        return sprintf('%04d-%02d-%02d', Add_Delta_Days(@datefrom, $enrolments->{enrolmentperiod}));
    } else {
        return $enrolments->{enrolmentperioddate};
    }
}

1;
