package Koha::Borrower::Search;

# Copyright 2015 Catalyst IT
#
# This file is part of Koha.
#
# Koha is free software; you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3 of the License, or (at your option) any later
# version.
#
# Koha is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with Koha; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

use Modern::Perl;

use C4::Context;
use Carp;

use base 'Exporter';

our @EXPORT_OK = qw( find_borrower find_borrower_from_ext get_borrower_fields );

=head1 NAME

Koha::Borrower::Search - a simple borrower searching interface

=head1 SYNOPSIS

    use Koha::Borrower::Search qw( find_borrower find_borrower_from_ext );
    # This will raise an exception if more than one borrower matches
    my $borr_num = find_borrower('cardnumber', '123ABC');
    # This will give you a list of all the numbers
    my @borr_nums = find_borrower('surname', 'Smith');

    my $borr_num = find_borrower_from_ext('MEMBERNUM', '54321');
    my @borr_nums = find_borrower_from_ext('DEPARTMENT', 'Ministry of Silly Walks');

=head1 DESCRIPTION

This provides some functions to find borrower records.

=head1 FUNCTIONS

=head2 find_borrower

    my $borr_num = find_borrower('cardnumber', '123ABC');
    my @borr_nums = find_borrower('surname', 'Smith');

Given a column in the borrower table and a value, this will find matching
borrowers. 

If called in a scalar context, an exception will be raised if there is more
than one result, or something else goes wrong, and C<undef> will be returned if
there are no results.

If called in a list context, all borrowernumbers are returned.

=cut

sub find_borrower {
    my ( $field, $value ) = @_;

    croak "Field value may not be safe for SQL" if $field =~ /[^A-Za-z0-9]/;
    my $dbh = C4::Context->dbh();
    my $q   = "SELECT borrowernumber FROM borrowers WHERE $field=?";
    my $sth = $dbh->prepare($q);
    $sth->execute($value);
    if (wantarray) {
        my @bibs;
        while (my $row = $sth->fetchrow_arrayref) {
            push @bibs, $row->[0];
        }
        return @bibs;
    } else {
        my $row = $sth->fetchrow_arrayref();
        return undef unless $row;
        my $bib = $row->[0];
        die "Multiple borrowers match provided values\n"
        if $sth->fetchrow_arrayref();
        return $bib;
    }
}

=head2 find_borrower_from_ext

    my $borr_num = find_borrower_from_ext('MEMBERNUM', '54321');
    my @borr_nums = find_borrower_from_ext('DEPARTMENT', 'Ministry of Silly Walks');

Given the code for an extended borrower attribute, and a value, this will
find the borrowers that match.

If called in a scalar context, an exception will be raised if there is more
than one result, or something else goes wrong, and C<undef> will be returned if
there are no results.

If called in a list context, all borrowernumbers are returned.

=cut

sub find_borrower_from_ext {
    my ( $code, $value ) = @_;

    my $dbh = C4::Context->dbh();
    my $q =
'SELECT DISTINCT borrowernumber FROM borrower_attributes WHERE code=? AND attribute=?';
    my $sth = $dbh->prepare($q);
    $sth->execute( $code, $value );
    if (wantarray) {
        my @bibs;
        while (my $row = $sth->fetchrow_arrayref) {
            push @bibs, $row->[0];
        }
        return @bibs;
    } else {
        my $row = $sth->fetchrow_arrayref();
        return undef unless $row;
        my $bib = $row->[0];
        die "Multiple borrowers match provided values\n"
        if $sth->fetchrow_arrayref();
        return $bib;
    }
}

=head2 get_borrower_fields

Fetches the list of columns from the borrower table. Returns a list.

=cut

sub get_borrower_fields {
    my $dbh = C4::Context->dbh();

    my @fields;
    my $q   = 'SHOW COLUMNS FROM borrowers';
    my $sth = $dbh->prepare($q);
    $sth->execute();
    while ( my $row = $sth->fetchrow_hashref() ) {
        push @fields, $row->{Field};
    }
    return @fields;
}

1;
