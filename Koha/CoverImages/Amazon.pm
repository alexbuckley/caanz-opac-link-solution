#
#===============================================================================
#
#         FILE: Amazon.pm
#
#  DESCRIPTION:
#
#        FILES: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Chris Cormack (rangi), chrisc@catalyst.net.nz
# ORGANIZATION: Koha Development Team
#      VERSION: 1.0
#      CREATED: 10/06/14 10:47:26
#     REVISION: ---
#===============================================================================

package Koha::CoverImages::Amazon;

# Copyright 2014 Catalyst <chrisc@catalyst.net.nz>
#
# This file is part of Koha.
#
# Koha is free software; you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3 of the License, or (at your option) any later
# version.
#
# Koha is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with Koha; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

use Modern::Perl;

use base qw(Koha::CoverImages);

sub checkimage {
    my $self         = shift;
    my $biblionumber = shift;
    my $size         = shift;
    my $isbn         = shift || $self->get_isbn_10($biblionumber);
    my $url = 'https://images-na.ssl-images-amazon.com/images/P/' . $isbn;
    if ( $size eq 'thumb' ) {
        $url = $url . '.01.TZZZZZZZ.jpg';
    }
    elsif ( $size eq 'large' ) {
        $url = $url . '.01.LZZZZZZZ.jpg';
    }
    else {
        $url = $url . '.01.TZZZZZZZ.jpg';
    }
    my $content_type = $self->testimage($url);
    if ($content_type) {
        return ( $url, $content_type );
    }
    return;
}

1;
