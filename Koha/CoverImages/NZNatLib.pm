#
#===============================================================================
#
#         FILE: NZNatLib.pm
#
#  DESCRIPTION:
#
#        FILES: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Chris Cormack (rangi), chrisc@catalyst.net.nz
# ORGANIZATION: Koha Development Team
#      VERSION: 1.0
#      CREATED: 11/06/14 13:19:06
#     REVISION: ---
#===============================================================================
package Koha::CoverImages::NZNatLib;

# Copyright 2014 Catalyst <chrisc@catalyst.net.nz>
#
# This file is part of Koha.
#
# Koha is free software; you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3 of the License, or (at your option) any later
# version.
#
# Koha is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with Koha; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

use Modern::Perl;

use base qw(Koha::CoverImages);

sub checkimage {
    my $self         = shift;
    my $biblionumber = shift;
    my $size         = shift;
    my $isbn         = shift;
    my $url;
    my $baseurl = 'http://digital.natlib.govt.nz/bookcover/get/';
    if ($isbn) {
        $url = $baseurl . "isbn/" . $isbn;
    }
    else {
    # try to get isbn, if not try for issn, then, ismn, then nbd (stored in 035)
    }
    my $content_type = $self->testimage($url);
    if ($content_type) {
        return ( $url, $content_type );
    }
    return;
}

sub testimage {

# we have to override this for Natlib  covers, they don't reliably hand back size
    my $self = shift;
    my $url  = shift;
    my $ua   = LWP::UserAgent->new;
    $ua->agent('Koha_Library_System');
    my $response = $ua->get($url);
    if ( $response->header('Content-Disposition')
        && ( $response->header('Content-Disposition') !~ /1x1\./ ) )
    {
        return $response->headers->content_type;
    }
    return;
}

1;

