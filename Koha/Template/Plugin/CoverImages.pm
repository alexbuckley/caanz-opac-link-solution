package Koha::Template::Plugin::CoverImages;

# Copyright 2012 ByWater Solutions
# Copyright 2013-2014 BibLibre
#
# Koha is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# Koha is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Koha; if not, see <http://www.gnu.org/licenses>.

use Modern::Perl;

use Template::Plugin;
use base qw( Template::Plugin );

use Koha::CoverImages;
use C4::Context;

sub image_url {
    my $self         = shift;
    my $biblionumber = shift;
    my $size         = shift;
    my $isbn         = shift;
    my $local        = C4::Context->preference('ServerCoverImages');
    my $url;
    if (! $local) {
        my $images = Koha::CoverImages->new( { cache => '' } );
        $url = $images->fetch_url( $biblionumber, $size, $isbn );
    }
    else {
        $url =
            "/cgi-bin/koha/opac-covers.pl?biblionumber="
          . $biblionumber
          . "&size="
          . $size
          . "&isbn="
          . $isbn;
    }
    return $url;
}

1;

=head1 NAME

Koha::Template::Plugin::CoverImages- TT Plugin for coverimages

=head1 SYNOPSIS

[% USE AuthorisedValues %]

[% AuthorisedValues.GetByCode( 'CATEGORY', 'AUTHORISED_VALUE_CODE', 'IS_OPAC' ) %]

[% AuthorisedValues.GetAuthValueDropbox( $category, $default ) %]

=head1 ROUTINES

=head2 GetByCode

In a template, you can get the description for an authorised value with
the following TT code: [% AuthorisedValues.GetByCode( 'CATEGORY', 'AUTHORISED_VALUE_CODE', 'IS_OPAC' ) %]

The parameters are identical to those used by the subroutine C4::Koha::GetAuthorisedValueByCode.

=head2 GetAuthValueDropbox

The parameters are identical to those used by the subroutine C4::Koha::GetAuthValueDropbox

=head1 AUTHOR

Chris Cormack <chrisc@catalyst.net.nz>

=cut
