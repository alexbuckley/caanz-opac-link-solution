#
#===============================================================================
#
#         FILE: CoverImages.pm
#
#  DESCRIPTION:
#
#        FILES: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Chris Cormack (rangi), chrisc@catalyst.net.nz
# ORGANIZATION: Koha Development Team
#      VERSION: 1.0
#      CREATED: 10/06/14 07:54:22
#     REVISION: ---
#===============================================================================

package Koha::CoverImages;

# Copyright 2014 Catalyst <chrisc@catalyst.net.nz>
#
# This file is part of Koha.
#
# Koha is free software; you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3 of the License, or (at your option) any later
# version.
#
# Koha is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with Koha; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

=head1 NAME

Koha::CoverImages

=head1 SYNOPSIS

  use Koha::CoverImages;
  my $images = Koha::CoverImages->new();
  my $image = $images->fetch($biblionumber)

=head1 FUNCTIONS

=cut

use Modern::Perl;
use Koha::Cache;
use base qw(Class::Accessor);
use LWP::UserAgent;
use Module::Load::Conditional qw(can_load);

use C4::Context;

Koha::CoverImages->mk_accessors(qw( cache imagesources ));

sub fetch {
    my $self         = shift;
    my $biblionumber = shift
      || die;    # we cant do anything without a biblionumber, bail out
    my $size = shift || 'thumbnail';
    my $isbn = shift;
    my $image_data;
    unless ( $image_data = $self->fetch_from_cache( $biblionumber, $size ) ) {
        $image_data = $self->fetch_from_sources( $biblionumber, $size, $isbn );
    }
    my ( $image, $mimetype ) = $self->display_image($image_data);
    return ( $image, $mimetype );
}

sub fetch_url {
    my $self         = shift;
    my $biblionumber = shift;
    my $size         = shift || 'thumbnail';
    my $isbn         = shift;
    my $image_data;
    unless ( $image_data = $self->fetch_from_cache( $biblionumber, $size ) ) {
        $image_data = $self->fetch_from_sources( $biblionumber, $size, $isbn );
    }
    return ( $image_data->{'url'} );
}

sub fetch_from_cache {
    my $self         = shift;
    my $biblionumber = shift;
    my $size         = shift;
    unless ( $self->{'cache'} ) {
        my $cache = $self->create_cache();
        $self->cache($cache);
    }
    my $cache = $self->{'cache'};
    my $ret =
      $cache->get_from_cache( "COVERIMAGE::" . $biblionumber . "::" . $size );
    return $ret;
}

sub create_cache {
    my $self  = shift;
    my $cache = Koha::Cache->get_instance();
    return $cache;
}

sub fetch_from_sources {
    my $self         = shift;
    my $biblionumber = shift;
    my $size         = shift;
    my $isbn         = shift;
    unless ( $self->{'imagesources'} ) {
        $self->imagesources( $self->get_sources() );
    }
    my $imagedata;
    my $sources = $self->{'imagesources'};
    foreach my $source (@$sources) {
        $source = "Koha::CoverImages::" . $source;
        if ( can_load( modules => { $source => undef } ) ) {
            my $x = $source->new();
            my ( $url, $mimetype ) =
              $x->checkimage( $biblionumber, $size, $isbn );
            if ($url) {
                $imagedata = { 'url' => $url, 'mimetype' => $mimetype };
                $self->set_in_cache( $biblionumber, $size, $imagedata );
                return $imagedata;
            }
        }
        else {
            warn "We could not load $source";
        }
    }
    return;    # need to return a url to notfound image
}

sub get_sources {
    my $self = shift;
    warn C4::Context->preference('CoverImageOrder');
    my @sources = split( /\|/, C4::Context->preference('CoverImageOrder') );
    return \@sources;
}

sub set_in_cache {
    my $self         = shift;
    my $biblionumber = shift;
    my $size         = shift;
    my $image_data   = shift;
    $self->cache->set_in_cache( "COVERIMAGE::" . $biblionumber . "::" . $size,
        $image_data, { 'expiry' => 60 } );    #store for a day

}

sub display_image {
    my $self       = shift;
    my $image_data = shift;
    unless ( $image_data->{'url'} ) {
        warn "No URL set";
        return;
    }
    my $ua = LWP::UserAgent->new();
    $ua->agent('Koha_Library_System');
    my $image = $ua->get( $image_data->{'url'} );
    if ( $image->is_success ) {
        return ( $image->content, $image_data->{'mimetype'} );
    }
    else {
        warn "No Image fetched for " . $image_data->{'url'};
        return;
    }
}

sub get_isbn_10 {
    my $self         = shift;
    my $biblionumber = shift;
}

sub testimage {
    my $self = shift;
    my $url  = shift;
    my $ua   = LWP::UserAgent->new;
    $ua->agent('Koha_Library_System');
    my $response = $ua->get($url);
    my $headers  = $response->headers;
    if ( $headers->content_length > 2048 ) {    #bigger than 2k
        return ( $headers->content_type );
    }
    return;
}

1;
