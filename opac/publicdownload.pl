#!/usr/bin/perl 
#===============================================================================
#
#         FILE: download.pl
#
#        USAGE: ./download.pl
#
#  DESCRIPTION:
#
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Chris Cormack (rangi), chrisc@catalyst.net.nz
# ORGANIZATION: Koha Development Team
#      VERSION: 1.0
#      CREATED: 25/02/14 14:15:07
#     REVISION: ---
#===============================================================================

use strict;
use warnings;

use C4::Auth;    # get_template_and_user
use C4::Context;
use C4::Biblio;
use CGI;
use LWP::Simple;
use LWP::UserAgent;

my $ua = LWP::UserAgent->new(
   ssl_opts => { verify_hostname => 0 },
);
my $input = CGI->new();
my ( $template, $borrowernumber, $cookie ) = get_template_and_user(
    {
        template_name   => "opac-main.tt",
        type            => "opac",
        query           => $input,
        authnotrequired => ( C4::Context->preference("OpacPublic") ? 1 : 0 ),

        #       authonotrequired => 0,
        flagsrequired => { borrow => 1 },
    }
);

my $biblionumber = $input->param('biblionumber') || die "no biblionumber";
my $record       = GetMarcBiblio($biblionumber)  || die "no record found";
my $marcflavour = C4::Context->preference("marcflavour");
my $link = $input->param('link') || '1';

my $marcurlsarray = GetMarcUrls( $record, $marcflavour );

my $url;
#Load the appropriate URL based on the value of the link parameter 
if ($link == 2) {
    my $url = $marcurlsarray->[1]->{'MARCURL'};
} else {
    my $url = $marcurlsarray->[0]->{'MARCURL'};
}

my $r =  $ua->head($url);
my $content_type = $r->header('Content-Type');
if ( $content_type =~ /html/ || $url =~ /bit\.ly/ ) {
    print $input->redirect( -uri => $url, -cookie => $cookie );
}
else {
    print $input->header( -type => $content_type, -cookie => $cookie );
    getprint($url);
}
