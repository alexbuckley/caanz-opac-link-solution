#!/usr/bin/perl 
#===============================================================================
#
#         FILE: download.pl
#
#        USAGE: ./download.pl
#
#  DESCRIPTION:
#
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Chris Cormack (rangi), chrisc@catalyst.net.nz
# ORGANIZATION: Koha Development Team
#      VERSION: 1.0
#      CREATED: 25/02/14 14:15:07
#     REVISION: ---
#===============================================================================

use strict;
use warnings;

use C4::Auth;    # get_template_and_user
use C4::Context;
use CGI;
use Digest::MD5 qw(md5_hex);

my $input = CGI->new();

my ( $template, $borrowernumber, $cookie ) = get_template_and_user(
    {
        template_name   => "opac-main.tt",
        type            => "opac",
        query           => $input,
        authnotrequired => ( C4::Context->preference("OpacPublic") ? 1 : 0 ),

        #       authonotrequired => 0,
        flagsrequired => { borrow => 1 },
    }
);

my $eblnumber = $input->param('eblnumber') || die "no eblnumber";
my $sharedsecret = C4::Context->preference('EBLSharedSecret');
my $url          = C4::Context->preference('EBLURL');
my $time         = time();

my $raw_data     = $borrowernumber . $time . $sharedsecret;
my $hash         = md5_hex($raw_data);

print $input->redirect($url . "p=$eblnumber&userid=$borrowernumber&tstamp=$time&id=$hash");

