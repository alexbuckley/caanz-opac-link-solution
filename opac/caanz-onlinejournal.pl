#!/usr/bin/perl 
#===============================================================================
#
#         FILE: download.pl
#
#        USAGE: ./download.pl
#
#  DESCRIPTION:
#
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Chris Cormack (rangi), chrisc@catalyst.net.nz
# ORGANIZATION: Koha Development Team
#      VERSION: 1.0
#      CREATED: 25/02/14 14:15:07
#     REVISION: ---
#===============================================================================

use strict;
use warnings;

use C4::Auth;    # get_template_and_user
use CGI;

my $input = CGI->new();

my ( $template, $borrowernumber, $cookie ) = get_template_and_user(
    {
        template_name   => "opac-main.tt",
        type            => "opac",
        query           => $input,
 #       authnotrequired => ( C4::Context->preference("OpacPublic") ? 1 : 0 ),

               authonotrequired => 0,
        flagsrequired => { borrow => 1 },
    }
);

print $input->redirect( -uri => "http://search.ebscohost.com/login.aspx?authtype=uid&user=s1240828&password=password&profile=ehost&defaultdb=plh" );
