#!/usr/bin/perl 
#===============================================================================
#
#         FILE: opac-covers.pl
#
#        USAGE: ./opac-covers.pl
#
#  DESCRIPTION:
#
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Chris Cormack (rangi), chrisc@catalyst.net.nz
# ORGANIZATION: Koha Development Team
#      VERSION: 1.0
#      CREATED: 10/06/14 13:36:53
#     REVISION: ---
#===============================================================================

use strict;
use warnings;

use Koha::CoverImages;
use CGI;

my $cgi          = CGI->new();
my $biblionumber = $cgi->param('biblionumber');
my $isbn         = $cgi->param('isbn');
my $size         = $cgi->param('size') || 'thumb';

my $images = Koha::CoverImages->new( { cache => '' } );
my ( $image, $mimetype ) = $images->fetch( $biblionumber, $size, $isbn );

if ($image) {
    print $cgi->header( -type => $mimetype, -expires => '+3d' );
    print $image;
}
else {
    print $cgi->header( -status=>404);
}
